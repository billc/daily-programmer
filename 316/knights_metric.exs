# Daily Programmer Challenge #316 [EASY] Knight's Metric
# https://www.reddit.com/r/dailyprogrammer/comments/6coqwk/20170522_challenge_316_easy_knights_metric/
#
defmodule KnightsMetric do
  @computed  %{
    {0,1} => [{2,1}, {-1, 2}, {-1,-2}],
    {1,0} => [{1,2}, {2,-1}, {-2,-1}],
    {0,-1} => [{2,-1}, {-1,2}, {-1,-2}],
    {-1,0} => [{-1,2}, {-2,-1},{2,-1}]
  }

  defp parse(input) do
    {_, [x, y], _} = OptionParser.parse(input, switches: [:integer, :integer])
    {String.to_integer(x), String.to_integer(y)}
  end

  def run(input) do
    solution =
      input
      |> parse
      |> move({0,0})

    IO.puts "Solution found in #{length(solution)} moves."
    IO.inspect solution
  end

  defp move(destination, current) when current == destination do
    []
  end

  defp move({xx, yy}, {x,y}) when (abs(xx - x) + abs(yy - y)) < 2  do
    @computed[{xx-x, yy-y}]
  end

  defp move({xx,yy} = destination, {x,y}) when abs(xx - x) >= abs(yy - y) do
    a = 2 * if (xx-x < 0), do: -1, else: 1
    b = if (yy-y < 0), do: -1, else: 1
    # IO.puts " --> (#{x + a}, #{y + b})"
    [{a,b} | move(destination, { x + a, y + b })]
  end


  defp move({xx, yy} = destination, {x,y}) when abs(xx - x)  < abs(yy - y )  do
    a = if (xx-x < 0), do: -1, else: 1
    b = 2 * if (yy-y < 0), do: -1, else: 1
    # IO.puts " --> (#{x + a}, #{y + b})"
    [{a,b} | move(destination, {x + a, y + b}) ]
  end
end

System.argv
|> KnightsMetric.run
