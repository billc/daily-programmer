# Daily Programmer Challenge 318 [Easy] Countdown Game Show
# https://www.reddit.com/r/dailyprogrammer/comments/6fe9cv/20170605_challenge_318_easy_countdown_game_show/
#
defmodule Solution do

  def permute([]), do: [[]]

  def permute(list) do
    for x <- list, y <- permute(list -- [x]), do: [x|y]
  end

  def permutate([h | t]) do
    for o <- [h], i <- _permutate(t), do: "#{o}#{i}"
  end

  def _permutate([h | t]) do
    for o <- ['+', '-', '*', '/'], i <- _permutate(t), do: "#{o}#{h}#{i}"
  end

  def _permutate([]), do: [""] 

  def _permutate(t) do
    for o <- ['+', '-', '*', '/'], i <- [t], do: "#{o}#{i}"
  end

  def compute(equation) do
    case Regex.run(~r/^(-?\d+)([\+\-\*\/])(\d+)/, equation) do
      [expr, x, op, y]  ->
        solve(expr, x, op, y) |> insert(expr, equation) |> compute
      nil ->
        String.to_integer(equation)
    end
  end

  def solve(expr, x, op, y) do
    {ans, _} = 
    if op == "/" do
      a = String.to_integer(x)
      b = String.to_integer(y)
      { div(a,b), []}
    else
      Code.eval_string(expr)
    end
    ans
  end

  def insert(ans, expr, equation) do
    String.replace(equation, expr, Integer.to_string(ans), global: :false)
  end

end

defmodule Countdown do
  import Solution

  def parse_input(input) do
    {answer, list} = List.pop_at(input, -1)

    values = Enum.map(list, &String.to_integer/1)

    {String.to_integer(answer), values}
  end

  def run(input) do
    {answer, values} = parse_input(input)

    solution = values
    |> permute
    |> Enum.map(&permutate/1)
    |> List.flatten
    |> Enum.filter(fn(x) -> compute(x) == answer end)

    IO.inspect solution
    IO.puts "#{length(solution)} solutions found"
  end
end

defmodule Benchmark do
  def measure(function) do
    function
    |> :timer.tc
    |> elem(0)
    |> Kernel./(1_000_000)
  end
end

elapsed = Benchmark.measure(fn -> Countdown.run(System.argv) end)
IO.puts "Solution took #{elapsed} seconds to complete"

